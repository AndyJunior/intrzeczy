const CACHE_NAME = 'myCache';
// List of files which are store in cache.
let filesToCache = [
 '/',
 '/style.css'
 ]
self.addEventListener('install', function (evt) {
    evt.waitUntil(
    caches.open(CACHE_NAME).then(function (cache) {
    return cache.addAll(filesToCache);
    }).catch(function (err) {
    // Snooze errors...
    // console.error(err);
    })
    );
   });
/* request is being made */
self.addEventListener('fetch', function(event) {
  event.respondWith(
    //first try to run the request normally
    fetch(event.request).catch(function() {
      //catch errors by attempting to match in cache
      return caches.match(event.request).then(function(response) {
        // Cache hit - return response
        if (response) {
          return response;
        }
      });
    })
  );
});